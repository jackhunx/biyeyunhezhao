//index.js
import CanvasDrag from '../../components/canvas-drag/canvas-drag';

Page({
  data: {
    graph: {},
    bg: '',
    pe: ''
  },

  // 添加背景图片
  addBg: function(bg) {
    wx.getImageInfo({
      src: bg,
      success: (res) => {
        console.log("获取背景的信息", res)
        CanvasDrag.changeBgImage(res.path);
      }
    })
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // this.setData({
    //   bg: options.bg,
    //   pe: options.pe
    // })
    wx.showLoading({
      title: '正在加载资源',
    })
    console.log("背景", options.bg)
    console.log("形象", options.pe)
    this.addBg(options.bg);
    this.onAddPeople(options.pe);

  },

  onShow: function() {

  },

  onReady: function () {


  },

  /**
   * 添加人物图片
   */
  onAddPeople(pe) {
    wx.getImageInfo({
      src: pe,
      success: (res) => {
        console.log("获取人物的信息",res)
        wx.hideLoading()
        this.setData({
          graph: {
            w: res.width * 0.1,
            h: res.height * 0.1,
            type: 'image',
            url: res.path,
          }
        });
      }
    })
    
  },

  /**
   * 生成图片
   */
  onExport() {
    wx.showLoading({
      title: '正在生成',
    })
    CanvasDrag.export()
      .then((filePath) => {
        console.log(filePath);
        wx.hideLoading()
        wx.navigateTo({
          url: '/pages/show/show?file=' + filePath,
        })
      })
      .catch((e) => {
        wx.hideLoading()
        console.error(e);
      })
  },


});
