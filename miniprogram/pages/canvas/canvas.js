//index.js
import CanvasDrag from '../../components/canvas-drag/canvas-drag';

Page({
  data: {
    graph: {},
  },


  // 添加背景图片
  // addBg: function() {
  //   CanvasDrag.changeBgImage('/images/11.jpg');
  // },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var that = this;
    // 添加白色背景
    CanvasDrag.changeBgColor('white');

    // 添加生成的图片和小程序码
    CanvasDrag.initByArr([{ "type": "bgColor", "color": "white" }, { "type": "image", "url": "/images/11.jpg", "y": -44.58187743883326, "x": 69.96218644231296, "w": 177.87556913197565, "h": 295.76377623997126, "rotate": -90, "sourceId": null }, { "type": "image", "url": "/images/ma.jpg", "y": 217.82440400571167, "x": 9.624456648533975, "w": 63.951076021779706, "h": 63.951076021779706, "rotate": 0, "sourceId": null }]);
    // 添加图片
    // this.setData({
    //   graph: {
    //     w: 341,
    //     h: 567   ,
    //     type: 'image',
    //     url: 'http://tmp/wx6952cb178710d0c5.o6zAJs_kcGLBaUDH4ayYCkDalQto.DsDUaMA9emLlb26d39c0b3e9fd1b1a4a6a22fdb0eefe.png',
    //   }
    // });
    // 添加二维码
    // this.setData({
    //   graph: {
    //     w: 100,
    //     h: 100,
    //     type: 'image',
    //     url: '/images/ma.jpg',
    //   }
    // });
  },

  // 保存到相册
  save: function () {
    CanvasDrag.export()
      .then((filePath) => {
        console.log(filePath);
        wx.previewImage({
          urls: [filePath]
        })
      })
      .catch((e) => {
        console.error(e);
      })
  },

  onShow: function () {

  },
  // cli: function() {
  //   CanvasDrag.exportJson()
  //     .then((imgArr) => {
  //       console.log(JSON.stringify(imgArr));
  //     })
  //   CanvasDrag.initByArr([{ "type": "bgColor", "color": "white" }, { "type": "image", "url": "/images/11.jpg", "y": -44.58187743883326, "x": 69.96218644231296, "w": 177.87556913197565, "h": 295.76377623997126, "rotate": -90, "sourceId": null }, {"type":"image","url":"/images/ma.jpg","y":217.82440400571167,"x":9.624456648533975,"w":63.951076021779706,"h":63.951076021779706,"rotate":0,"sourceId":null}]);

  // },

  onReady: function () {


  },

  // /**
  //  * 添加人物图片
  //  */
  // onAddPeople() {
  //   wx.getImageInfo({
  //     src: '/images/男博士（粉）.png',
  //     success: (res) => {
  //       // console.log(res)

  //       this.setData({
  //         graph: {
  //           w: res.width * 0.1,
  //           h: res.height * 0.1,
  //           type: 'image',
  //           url: '/images/男博士（粉）.png',
  //         }
  //       });
  //     }
  //   })

  // },

  /**
   * 添加图片
   */
  onAddImage() {

    wx.chooseImage({
      success: (res) => {
        console.log(res)
        this.setData({
          graph: {
            w: 200,
            h: 200,
            type: 'image',
            url: res.tempFilePaths[0],
          }
        });
      }
    })
  },


  /**
   * 生成图片
   */
  onExport() {
    CanvasDrag.export()
      .then((filePath) => {
        console.log(filePath);
        wx.navigateTo({
          url: '/pages/show/show?file=' + filePath,
        })
        // wx.previewImage({
        //   urls: [filePath]
        // })
      })
      .catch((e) => {
        console.error(e);
      })
  },



  onImport() {
    // 有背景
    // let temp_theme = [{"type":"bgColor","color":"yellow"},{"type":"image","url":"../../assets/images/test.jpg","y":98.78423143832424,"x":143.78423143832424,"w":104.43153712335152,"h":104.43153712335152,"rotate":-12.58027482265038,"sourceId":null},{"type":"text","text":"helloworld","color":"blue","fontSize":24.875030530031438,"y":242.56248473498428,"x":119.57012176513672,"w":116.73966979980469,"h":34.87503053003144,"rotate":8.873370699754087}];
    // 无背景
    let temp_theme = [{
      "type": "image",
      "url": "../../assets/images/test.jpg",
      "y": 103,
      "x": 91,
      "w": 120,
      "h": 120,
      "rotate": 0,
      "sourceId": null
    }, {
      "type": "text",
      "text": "helloworld",
      "color": "blue",
      "fontSize": 20,
      "y": 243,
      "x": 97,
      "rotate": 0
    }];

    CanvasDrag.initByArr(temp_theme);
  },

  onClearCanvas: function (event) {
    let _this = this;
    _this.setData({
      canvasBg: null
    });
    CanvasDrag.clearCanvas();
  },

  onUndo: function (event) {
    CanvasDrag.undo();
  }
});