// miniprogram/pages/show/show.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    file: ''
  },


  // 保存到相册
  save: function() {
    wx.saveImageToPhotosAlbum({
      filePath: this.data.file, 
      success(res) {
        wx.showModal({
          title: '提示',
          content: '保存成功，再来一张？',
          success(res) {
            if(res.confirm) {
              wx.redirectTo({
                url: '/pages/people/people',
              })
            }
          }
        })
        console.log("success");
      },
      fail: function (res) {
        console.log(res);
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    console.log(options)
    console.log(options.file)
    this.setData({
      file: options.file,
      imgWidth: 409.2,
      imgHeight: 680.4
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})